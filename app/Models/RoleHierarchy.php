<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleHierarchy extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id', 'hierarchy'
    ];
    protected $table = 'role_hierarchy';
    public $timestamps = false;
    
}
