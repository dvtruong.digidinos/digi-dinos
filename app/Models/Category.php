<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Category extends Model
{
    protected $table = 'category';
    public $timestamps = false;

    public function posts() {
        return $this->hasMany('App\Models\Posts');
    }
}
