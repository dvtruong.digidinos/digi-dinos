<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Posts extends Model
{
 
    protected $table = 'posts';
    public $timestamps = false;

    public function category() {
        return $this->hasOne('App\Models\Category');
    }

    public function user() {
        return $this->belongsto('App\User', 'users_id');
    }
}
