<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreBlogPost;
use App\Http\Requests\CategoryValidate;
use App\Models\Category;
use Carbon\Carbon;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $data = Category::paginate(10);
      return view('dashboard.category.list', ['category'=>$data, 'data'=> $data]);
    }

    /**
     * Function search
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request) {
      $id = $request->id;
      $name = $request->name;
      $type = $request->type;
      $start_at = $request->get('start_at');
      $end_at = $request->get('end_at');
      $description = $request->description;

      $categories = Category::orderBy('id', 'desc');

      if ($id) {
        $categories = $categories->where('id', 'like', '%'. $id .'%');
      }

      if ($name) {
        $categories = $categories->where('name', 'like', '%'. $name .'%');
      }

      if ($type != null || $type != '') {
        $categories = $categories->where('type', 'like', '%'. $type .'%');
      }

      if ($start_at && $end_at) {
        $posts = $posts->whereBetween('created_at', array($start_at,$end_at));
      }

      if ($description) {
          $categories = $categories->where('description', 'like', '%'. $description .'%');
      }


      $data = Category::with('posts')->get();
      return view('dashboard.category.list', ['data'=>$categories->paginate(10), 'category'=> $data]);
    }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
    public function create()
    {
      $data = Category::get();
      return view('dashboard.category.create', ['data'=> $data]);
    }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
    public function store(CategoryValidate $request)
    {
      $category =  new Category();
      $category->name =  $request->name;
      $category->type = $request->type;
      $category->description = $request->description;
      $category->created_at = Carbon::now();
      $category->updated_at = Carbon::now();

      try {
        $category->save();
        return redirect('/admin/category/list');
      }
      catch (Exception $e) {
        report($e);

        return false;
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Category::where('id','=', $id)->get();
        $category = Category::get();
        foreach ($data as $key => $value) {
          $dataEdit = $value;
        };
        return view('dashboard.category.create', ['dataEdit'=>$dataEdit, 'id'=>$id, 'category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryValidate $request, $id)
    {
        $category = Category::find($id);
        $category->name =  $request->name;
        $category->type = $request->type;
        $category->description = $request->description;
        $category->updated_at = Carbon::now();

        try {
          $category->save();
          return redirect('/admin/category/list');
        } catch (Exception $e) {
          report($e);
          return false;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data = Category::find($id);

        $data->delete();

        return redirect('admin/category/list');
    }
}
