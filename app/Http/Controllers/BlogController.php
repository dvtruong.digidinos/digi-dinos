<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\Posts;
use App\User;
use App\Models\Category;
use App\Http\Requests\FileRequest;
use App\Http\Requests\StoreBlogPost;


class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Posts::with('user')->orderBy('updated_at', 'desc')->paginate(10);
        $auth = User::get(['id', 'name']);
        $category = Category::get();
        return view('dashboard.blogs.list', ['data' => $data, 'auth' => $auth, 'category' => $category]);
    }

    /**
     * Function search
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request) {

      $auth = User::get();
      $category = Category::get();
      $id =  $request->get('id');
      $title = $request->get('title');
      $users_id = $request->get('users_id');
      $description = $request->get('description');
      $start_at = $request->get('start_at');
      $end_at = $request->get('end_at');
      $status = $request->get('status');
      $category_id = $request->get('category');
      $posts = Posts::orderBy('id', 'desc');

      if ($id) {
          $posts = $posts->where('id', 'like', '%'. $id .'%');
      }

      if ($title) {
          $posts = $posts->where('title', 'like', '%' . $title . '%');
      }

      if ($users_id) {
          $posts = $posts->where('users_id', 'like', '%' . $users_id. '%');
      }

      if ($description) {
          $posts = $posts->where('description', 'like', '%' . $description. '%');
      }

      if ($start_at && $end_at) {
          $posts = $posts->whereBetween('created_at', array($start_at,$end_at));
      }

      if ($category_id) {
          $posts = $posts->where('category_id', 'like', '%' . $category_id. '%');
      }
      if ($status != null || $status != '') {
        $posts = $posts->where('status', 'like', '%' . $status. '%');
      }

      return view('dashboard.blogs.list', ['data' => $posts->paginate(10), 'auth'=> $auth , 'category'=>$category]);
    }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
    public function create()
    {
      $auth = User::get(['id', 'name']);
      $category = Category::get();
      return view('dashboard.blogs.create', ['auth' => $auth, 'category'=> $category]);
    }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
    public function store(StoreBlogPost $request)
    {
      $post = new Posts();
      $file = $request->file('filename');
      if($file) {
        $file->move('upload', $file->getClientOriginalName());
        $post->head_image = 'upload/' . $file->getClientOriginalName();
      }
      $post->title = $request->title;
      $post->users_id = $request->users_id;
      $post->description = $request->description;
      $post->content = $request->content;
      $post->updated_at = Carbon::now();
      $post->status = $request->status;
      $post->category_id = $request->category;
      try {
        $post->save();
      }
      catch (Exception $e) {
        throw new Exception($e);
      }

      return redirect('/admin/blog/list');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = Posts::with('user')->join('category', 'posts.category_id', '=', 'category.id')->where('posts.id', '=', $id)->get();
      $auth = User::get(['id', 'name']);
      $category = Category::get();
      $post = null;
      foreach ($data as $key => $item) {
        $post = $item;
      };

      return view('dashboard.blogs.create', ['data'=>$post, 'id'=>$id, 'auth' => $auth, 'category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $post = Posts::find($id);
      $post->title = $request->title;
      $post->users_id = $request->users_id;
      $post->description = $request->description;
      $post->content = $request->content;
      $post->status = $request->status;
      $post->category_id = $request->category;
      $file= $request->file('filename');

      if($file) {
        $file->move('upload', $file->getClientOriginalName());
        $post->head_image = 'upload/' . $file->getClientOriginalName();
      }

      $post->updated_at = Carbon::now();
      try {
        $post->save();
      }
      catch (Exception $e) {
        throw new Exception();
      }
      return redirect('admin/blog/list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      try {
        $data = Posts::findOrFail($id);
        $data->delete();

        return redirect('admin/blog/list');
      }
      catch (ModelNotFoundException $exception) {
        dd(back()->withError($exception->getMessage())->withInput());
        return back()->withError($exception->getMessage())->withInput();
      }

    }
}
