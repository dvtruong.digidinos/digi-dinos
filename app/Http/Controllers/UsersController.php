<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Roles;
use App\UsersInformation;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $you = auth()->user();
        $users = User::all();
        return view('dashboard.admin.usersList', compact('users', 'you'));
    }

    /**
     * Get information user in database
     * 
     * @param number $id - id of user
     */
    public function profile($id) {
        $data = User::with('userInfor')->where('users.id', '=', $id)->get();
        return view('dashboard.admin.profile', ['information'=> $data]);
    }

    /**
     * Update information of user
     * 
     * @param array $request - form request
     * @param number $id - id of user
     */
    public function updateProfile(Request $request, $id) {

        $user = User::with('userInfor')->find($id);
        $user->name = $request->name;
        $user->userInfor->date_birth = $request->date_birth;
        $user->userInfor->marital = $request->marital;
        $user->userInfor->number_phone = $request->number_phone;
        $user->userInfor->address = $request->address;
        $file = $request->file('filename');
        if($file) {
            $file->move('upload', $file->getClientOriginalName());
            $user->image = 'upload/' . $file->getClientOriginalName();
        }
        $user->userInfor->description = $request->description;
        $user->push();
        return redirect('/admin');
    }

    /**
     * Check and change password of user
     *
     * @param array $request - form request
     */
    public function changePassword(Request $request) {
        $id = Auth::user()->id;
        $user = User::findOrFail($id);
        $current = $request->password_old;
        $this->validate($request, [
            'password_old' => 'required',
            'password_new' => 'required|min:8',
            'password_confirm' => 'required|min:8'
        ]);

        if(!Hash::check($current, $user->password)) {
            return back()->withErrors(['Password is not verify!']);
        }
        elseif( $request->password_new != $request->password_confirm) {
            return back()->withErrors(['The password confirm and password new is different.']);
        }else {
            $user->password = bcrypt($request->password_new);
            $user->save();
            return redirect('/admin');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('dashboard.admin.userShow', compact( 'user' ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $role = Roles::get();

        return view('dashboard.admin.userEditForm', compact('user', 'role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name'       => 'required|min:1|max:256',
            'email'      => 'required|email|max:256'
        ]);
        $user = User::find($id);
        $user->menuroles  = implode(",", $request->input("roles"));
        $user->name       = $request->input('name');
        $user->email      = $request->input('email');
        $user->status     = $request->status;
        $user->save();
        $request->session()->flash('message', 'Successfully updated user');
        return redirect()->route('member.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if($user){
            $user->delete();
        }
        return redirect()->route('member.index');
    }
}
