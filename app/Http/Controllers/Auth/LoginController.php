<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\RedirectResponse;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
      logout as performLogout;
      validateLogin as preformAttempLogin;
      sendFailedLoginResponse as performLogin;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request)
    {
      $this->performLogout($request);
      return redirect('/login');
    }
    public function validateLogin(Request $request) {
      $status = User::where('email', '=', $request->email)->get('status');
      foreach ($status as $key => $value) {
        if($value->status == 1 || $value->status == 3) {
          $this->performLogin($request);
          return route('login');
        }
      }
    }
}
