<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreBlogPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'filename' => 'mimes:jpeg,jpg,png,gif|required|max:50000',
            'description' => 'required|max:255',
            'content' =>  'required',
            'category' => 'required',

        ];
    }
    public function messages() {
        return [
            'title.required' => 'Title is also required',
            'title.max' => 'Title has not more than 255 characters',
            'name.max' => 'Name has not more than 255 characters',
            'description.required' => 'Description is also required',
            'description.max'=> 'Title has not more than 255 characters'
        ];
    }
}
