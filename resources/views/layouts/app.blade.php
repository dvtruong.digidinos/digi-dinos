<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
     <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <title>Digi Dinos Blogs</title>
    <link rel="apple-touch-icon" sizes="57x57" href="https://digidinos.com/img/logo.png">
    <link rel="apple-touch-icon" sizes="60x60" href="https://digidinos.com/img/logo.png">
    <link rel="apple-touch-icon" sizes="72x72" href="https://digidinos.com/img/logo.png">
    <link rel="apple-touch-icon" sizes="76x76" href="https://digidinos.com/img/logo.png">
    <link rel="apple-touch-icon" sizes="114x114" href="https://digidinos.com/img/logo.png">
    <link rel="apple-touch-icon" sizes="120x120" href="https://digidinos.com/img/logo.png">
    <link rel="apple-touch-icon" sizes="144x144" href="https://digidinos.com/img/logo.png">
    <link rel="apple-touch-icon" sizes="152x152" href="https://digidinos.com/img/logo.png">
    <link rel="apple-touch-icon" sizes="180x180" href="https://digidinos.com/img/logo.png">
    <link rel="icon" type="image/png" sizes="192x192" href="https://digidinos.com/img/logo.png">
    <link rel="icon" type="image/png" sizes="32x32" href="https://digidinos.com/img/logo.png">
    <link rel="icon" type="image/png" sizes="96x96" href="https://digidinos.com/img/logo.png">
    <link rel="icon" type="image/png" sizes="16x16" href="https://digidinos.com/img/logo.png">
    <link rel="manifest" href="assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="assets/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- Icons-->
    <link href="{{ asset('css/free.min.css') }}" rel="stylesheet"> <!-- icons -->
    <link href="{{ asset('css/flag-icon.min.css') }}" rel="stylesheet"> <!-- icons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <!-- Main styles for this application-->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/pace.min.css') }}" rel="stylesheet">

    @yield('css')
    {{-- Jquery --}}
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

    <link href="{{ asset('css/coreui-chartjs.css') }}" rel="stylesheet">
    {{-- ckeditor --}}
    <script src="https://cdn.ckeditor.com/4.13.1/standard-all/ckeditor.js"></script>
    <link href="path/to/highlight.js/styles/monokai_sublime.css" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <div class="logo-header">
                    <img src="https://digidinos.com/img/logo.png" height="60px" width="90px" style="padding-bottom: 10px" alt="Digi Dinos">
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
