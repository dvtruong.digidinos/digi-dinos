<!DOCTYPE html>
<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <title>Digi Dinos Blogs</title>
    <link rel="apple-touch-icon" sizes="57x57" href="https://digidinos.com/img/logo.png">
    <link rel="apple-touch-icon" sizes="60x60" href="https://digidinos.com/img/logo.png">
    <link rel="apple-touch-icon" sizes="72x72" href="https://digidinos.com/img/logo.png">
    <link rel="apple-touch-icon" sizes="76x76" href="https://digidinos.com/img/logo.png">
    <link rel="apple-touch-icon" sizes="114x114" href="https://digidinos.com/img/logo.png">
    <link rel="apple-touch-icon" sizes="120x120" href="https://digidinos.com/img/logo.png">
    <link rel="apple-touch-icon" sizes="144x144" href="https://digidinos.com/img/logo.png">
    <link rel="apple-touch-icon" sizes="152x152" href="https://digidinos.com/img/logo.png">
    <link rel="apple-touch-icon" sizes="180x180" href="https://digidinos.com/img/logo.png">
    <link rel="icon" type="image/png" sizes="192x192" href="https://digidinos.com/img/logo.png">
    <link rel="icon" type="image/png" sizes="32x32" href="https://digidinos.com/img/logo.png">
    <link rel="icon" type="image/png" sizes="96x96" href="https://digidinos.com/img/logo.png">
    <link rel="icon" type="image/png" sizes="16x16" href="https://digidinos.com/img/logo.png">
    <link rel="manifest" href="assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="assets/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- Icons-->
    <link href="{{ asset('css/free.min.css') }}" rel="stylesheet"> <!-- icons -->
    <link href="{{ asset('css/flag-icon.min.css') }}" rel="stylesheet"> <!-- icons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <!-- Main styles for this application-->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/pace.min.css') }}" rel="stylesheet">

    @yield('css')
    {{-- Jquery --}}
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    {{-- ckeditor --}}
    <script src="https://cdn.ckeditor.com/4.13.1/standard-all/ckeditor.js"></script>
    <link href="path/to/highlight.js/styles/monokai_sublime.css" rel="stylesheet">
  </head>

  <body class="c-app">
    @include('dashboard.webs.header')
    <div class="wrapper-content">
      <div class="container main-content">
        <div class="row justify-content-center">
          <div class="col-md-8">
              <div class="card">
                  <div class="card-header">{{ __('Waiting for account varification  ') }}</div>
                  <div class="card-body">
                    <div>
                      {{ __('Before proceeding, please waiting for account verification.') }}''
                    </div>
                  </div>
                  <a type="submit" class="btn btn-link p-0 m-0 align-baseline" href="/">{{ __('Click here to home page') }}</a>.
              </div>
          </div>
        </div>
  
      <!-- CoreUI and necessary plugins-->
      <script src="{{ asset('js/pace.min.js') }}"></script> 
      <script src="{{ asset('js/coreui.bundle.min.js') }}"></script>
      <script src="{{ asset('js/customJquery.js') }}"></script>
  
      @yield('javascript')
      </div>
      @include('dashboard.webs.footer')
    </div>

  </body>
</html>
