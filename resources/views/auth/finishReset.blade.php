@extends('layouts.app')
@section('content')
  <div class="container">
       <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><strong>{{ __('An email has been sent to you') }}</strong></div>

                <div class="card-body  ">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                          {{ __('Please check your email to begin reseting your password.') }}
                        </div>
                    @endif

                    {{ __('Reset password completed!') }}
                  </div>
                  <div class="card-footer">
                    <a href="/login" class="btn btn-info btn-square"> Go to Login</a>
                  </div>
            </div>
        </div>
    </div>
  </div>
@endsection