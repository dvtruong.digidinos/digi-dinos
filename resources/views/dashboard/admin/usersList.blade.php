@extends('dashboard.blog')

@section('content')
        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-header">
                      Member Management
                     </div>
                    <div class="card-body">
                        <table class="table table-responsive-sm table-striped">
                        <thead>
                          <tr>
                            <th>Username</th>
                            <th>E-mail</th>
                            <th>Roles</th>
                            <th>Verified at</th>
                            <th>Status</th>
                            <th>View</th>
                            <th>Edit</th>
                            <th>Delete</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($users as $user)
                            <tr>
                              <td>{{ $user->name }}</td>
                              <td>{{ $user->email }}</td>
                              <td>{{ $user->menuroles }}</td>
                              <td>{{ $user->email_verified_at }}</td>
                              <td class="">
                                @if($user->status == 2)
                                  <span class="badge badge-success">Verified</span>
                                @elseif($user->status == 1)
                                  <span class="badge badge-warning">Verify Wait</span>
                                @elseif($user->status == 3)
                                  <span class="badge badge-danger">Cancelled</span>
                                @endif
                              </td>
                              <td>
                                <a href="{{ url('/admin/member/' . $user->id) }}" class="btn btn-square btn-success"><i class="fas fa-eye"></i></a>
                              </td>
                              <td>
                                <a href="{{ url('/admin/member/' . $user->id . '/edit') }}" class="btn btn-square btn-success"><i class="fas fa-edit"></i></a>
                              </td>
                              <td>
                                @if( $you->id !== $user->id )
                                <form action="{{ route('member.destroy', $user->id ) }}" method="POST">
                                  @method('DELETE')
                                  @csrf
                                  <button class="btn btn-square btn-danger " type="button" data-toggle="modal" data-target="#dangerModal-{{ $user->id}}"><i class="fas fa-trash-alt"></i></button>
                                  <div class="modal fade" id="dangerModal-{{ $user->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-danger" role="document">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <h4 class="modal-title">Save Blog</h4>
                                          <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                          <p>Are you sure you want to save changes?</p>
                                        </div>
                                        <div class="modal-footer">
                                          <button class="btn btn-secondary btn-square col-md-2" type="button" data-dismiss="modal">Close</button>
                                          <button type="submit" class="btn btn-square btn-danger col-md-2" value="upload">Save</button>
                                        </div>
                                      </div>
                                      <!-- /.modal-content-->
                                    </div>
                                    <!-- /.modal-dialog-->
                                  </div>
                                </form>
                                @endif
                              </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection


@section('javascript')

@endsection
