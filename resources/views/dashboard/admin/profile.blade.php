@extends('dashboard.blog')
@section('content')
<div class="container-fluid">
   <!-- /.row-->
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header"><strong>Member</strong> Information</div>
        @if(isset($information))
        @foreach ($information as $item)
        <form class="form-horizontal" action="{{ route('updateProfile', $item->id) }}" method="POST" enctype="multipart/form-data">
          @method('PATCH')
          @csrf
        <div class="card-body">
            <div class="form-group row">
              <label class="col-md-3 col-form-label">Name</label>
              <div class="col-md-9">
               <input type="text" class="form-control" name="name" id="name" placeholder="Enter Your Name" value="{{ $item->name }}">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="email">Email</label>
              <div class="col-md-9">
                <input class="form-control" id="email-input" type="email" disabled name="email" placeholder="Enter Email" autocomplete="email" value="{{ $item->email }}">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="date_birth">Date of birth</label>
              <div class="col-md-9">
                <input class="form-control" id="date_birth" type="date" name="date_birth" placeholder="Date of birth" value="{{ date("Y-m-d", strtotime($item->userInfor->date_birth))}}">
              </div>
            </div>
            
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="">Marital Status</label>
              <div class="col-md-9">
                <div class="form-check form-check-inline mr-1">
                  <input class="form-check-input" id="inline-radio1" {{ $item->userInfor->marital == 0 ? 'checked': '' }} type="radio" value="0" name="marital">
                  <label class="form-check-label" for="inline-radio1">Single</label>
                </div>
                <div class="form-check form-check-inline mr-1">
                <input class="form-check-input" id="inline-radio2" {{ $item->userInfor->marital == 1 ? 'checked': '' }} type="radio" value="1" name="marital">
                  <label class="form-check-label" for="inline-radio2">Married</label>
                </div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="inputError2">Image</label>
              <div class="col-md-9">
                <input type="file" name="filename" class="file" accept="image/*">
                  <div class="input-group my-3">
                    <input type="text" class="form-control col-sm-3" disabled placeholder="Upload File" id="file">
                    <div class="input-group-append" >
                      <button type="button" class="browse btn btn-primary" >Browse...</button>
                    </div>
                  </div>
                  <div class="ml-2 col-sm-6" >
                    <img src="" id="preview" class="img-thumbnail browse img-thumbnail-disabled">
                    @if(isset($item) && $item->image)
                    <img src=" {{ url($item->image) }}" id="preview" class="img-thumbnail is-edit browse">
                    @else
                    <img src=" {{ asset('assets/img/no-image.png') }}" id="preview" class="img-thumbnail no-image browse">
                    @endif
                  </div>
                </div>
              </div>


            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="number_phone">Number Phone</label>
              <div class="col-md-9">
                <input class="form-control" id="number-phone" type="number" name="number_phone" placeholder="Enter Number Phone" value="{{ $item->userInfor->number_phone}}">
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="address">Address</label>
              <div class="col-md-9">
                <textarea class="form-control" id="address" name="address" rows="9" placeholder="Enter number of house, street, district..">{{ $item->userInfor->address }}</textarea>
              </div>
            </div>
            
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="description">Descriptions</label>
              <div class="col-md-9">
                <textarea class="form-control" id="description" name="description" rows="9" placeholder="Enter Descriptions..">{{ $item->userInfor->description }}</textarea>
              </div>
            </div>
            <div class="card-footer">
              <button class="btn btn-square btn-success btn-save" type="button" >Save</button>
                <div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-success" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title">Save Blog</h4>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                      </div>
                      <div class="modal-body">
                        <p>Are you sure you want to save changes?</p>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-secondary btn-square col-md-2" type="button" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-square btn-success col-md-2" value="upload">Save</button>
                      </div>
                    </div>
                    <!-- /.modal-content-->
                  </div>
                  <!-- /.modal-dialog-->
                </div>
              <button class="btn btn-square btn-danger" type="reset"> Reset</button>
            </div>
             @endforeach
            @endif
        </form>
      </div>
  <!-- /.row-->
    </div>
</div>
@endsection