@extends('dashboard.blog')

@section('content')
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
          <div class="card">
              <div class="card-header">
                {{ __('Edit') }} {{ $user->name }}</div>
                <div class="card-body">
                  <br>
                  <form method="POST" action="/admin/member/{{ $user->id }}">
                    @csrf
                    @method('PUT')
                    <div class="input-group row">
                      <label class="col-md-3 col-form-label" for="email">Name</label>
                       <div class="col-md-9 col-form-label">
                         <input class="form-control" type="text" placeholder="{{ __('Name') }}" name="name" value="{{ $user->name }}" required autofocus>
                       </div>
                    </div>
                    <div class="input-group row">
                      <label class="col-md-3 col-form-label" for="email">Email</label>
                       <div class="col-md-9 col-form-label">
                         <input class="form-control" type="text" placeholder="{{ __('E-Mail Address') }}" name="email" value="{{ $user->email }}" required>
                      </div>
                    </div>
                    <div class="input-group row">
                      <label class="col-md-3 col-form-label" for="email">Roles</label>
                      <div class="col-md-9 col-form-label">
                        @foreach ($role as $item)
                          <div class="form-check form-check-inline mr-1">
                            <input 
                              class="form-check-input"
                              name="roles[{{$item->id}}]"
                              id="inline-checkbox[{{$item->id}}]" type="checkbox" {{ substr_count($user->menuroles, $item->name) ? 'checked' : '' }}
                              value="{{ $item->name }}"
                            >
                            <label class="form-check-label" for="inline-checkbox[{{$item->id}}]">{{ $item->name }}</label>
                          </div>
                        @endforeach
                      </div>
                    </div>

                    <div class="input-group row">
                      <label class="col-md-3 col-form-label" for="email">Verify</label>
                      <div class="col-md-9 col-form-label" id="" name="status">
                        <div class="form-check form-check-inline mr-5">
                          <input type="radio" name="status"  {{ $user->status != 1 ? 'disabled' : '' }} {{ $user->status == 1 ? 'checked' : '' }} value="1">
                          &nbsp;
                          <span class="col-form-label">Verify Wait</span>
                        </div>
                        <div class="form-check form-check-inline mr-5">
                          <input type="radio" name="status" {{ $user->status == 3 ? 'disabled' : '' }} {{ $user->status == 2 ? 'checked' : '' }} value="2">
                          &nbsp;
                          <span class="col-form-label">Verified</span>
                        </div>
                        <div class="form-check form-check-inline mr-5">
                          <input type="radio" name="status" {{ $user->status == 3 ? 'disabled' : '' }} {{ $user->status == 3 ? 'checked' : '' }} value="3" >
                          &nbsp;
                          <span class="col-form-label">Canceled</span>
                        </div>
                      </div>
                    </div>
                    <div class="card-footer">
                      <button class="btn btn-square btn-success" type="submit">{{ __('Save') }}</button>
                      <a href="{{ route('member.index') }}" class="btn btn-square btn-danger">{{ __('Cancel') }}</a>
                    </div>
                  </form>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('javascript')

@endsection