@extends('dashboard.blog')
@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <form action="{{ route('changepassword') }}" method="POST">
          @csrf
          @method('PATCH')
        <div class="card-header">
          <strong>Member</strong> Information
        </div>
        <div class="card-body">
            @if(count($errors) > 0)
              <div class="alert alert-danger" role="alert">
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
              </div>
              @endif
            <div class="form-group row">
              <label class="col-md-3 col-form-label">Current Password</label>
              <div class="col-md-6">
                <input type="password" class="form-control" name="password_old" id="name" placeholder="Enter Current Password" required value="{{ old('password_old')}}">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-3 col-form-label">New Password</label>
              <div class="col-md-6">
               <input type="password" class="form-control" name="password_new" id="name" placeholder="Enter New Password" required value="">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-3 col-form-label"> Confirm Password</label>
              <div class="col-md-6">
               <input type="password" class="form-control" name="password_confirm" id="name" placeholder="Enter Confirm Password" required value="">
              </div>
            </div>
            </div>
            <div class="card-footer">
              <button class="btn btn-square btn-success btn-save" type="button" >Save</button>
                <div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-success" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title">Save Blog</h4>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                      </div>
                      <div class="modal-body">
                        <p>Are you sure you want to save changes?</p>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-secondary btn-square col-md-2" type="button" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-square btn-success col-md-2" value="upload">Save</button>
                      </div>
                    </div>
                    <!-- /.modal-content-->
                  </div>
                  <!-- /.modal-dialog-->
                </div>
              <button class="btn btn-square btn-danger" type="reset"> Cancel</button>
            </div>
          </form>
        </div>
    </div>
  </div>
</div>
@endsection