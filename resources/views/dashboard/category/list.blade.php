@extends('dashboard.category')

@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">Search Blog</div>
          <div class="card-body">
            <form class="form-horizontal" action="{{ route('admin.category.search') }}" method="get" enctype="multipart/form-data">

              <div class="form-group row">
                <label class="col-md-1 col-form-label" for="text-input">Name</label>
                <div class="col-md-11">
                  <input class="form-control" id="text-input" type="text" name="name" placeholder="Category Name">
                </div>
              </div>
              <div class="collapse" id="collapseExample">
                {{-- content --}}

                  <div class="form-group row">
                    <label class="col-md-1 col-form-label" for="text-input">Type</label>
                    <div class="col-md-11">
                      <select name="type"class="form-control " id="ccmonth" >
                        <option value=""></option>
                        <option value="0">News</option>
                        <option value="1">Blog</option>
                        <option value="2">Magazine</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-md-1 col-form-label" for="date-input">Start At</label>
                    <div class="col-md-11">
                      <input name="start_at" class="form-control" id="date-input" type="date" name="date-input" placeholder="">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-md-1 col-form-label" for="date-input">End At</label>
                    <div class="col-md-11">
                      <input name="end_at" class="form-control" id="date-input" type="date" name="date-input" placeholder="">
                    </div>
                  </div>

                </div>
                <button class="btn btn-square btn-dark" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="fas fa-plus"></i><span class="search-expand">&nbsp;Expand</span></a></button>
                <div class="form-group row">
                  <div class="btn-search">
                    <button type="submit" class="btn btn-square btn-success">Search &nbsp;<i class="fas fa-search"></i></button>
                  </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-sm-8 col-md-10 col-lg-12 col-xl-12">
          <div class="card">
              <div class="card-header">
                Category List
              </div>
              <div class="card-body">
                  <table class="table table-responsive-sm">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th >Descriptions</th>
                      <th>Created At</th>
                      <th>Updated At</th>
                      <th>Edit</th>
                      <th>Delete</th>
                    </tr>
                  </thead>
                  <tbody>
                    @if (isset($data))
                        @foreach ($data as $item)
                        <tr>
                          <td>{{ $item->id }}</td>
                          <td>{{ $item->name }}</td>
                          <td style="max-width: 400px">{{ $item->description }}</td>
                          <td>{{ $item->created_at }}</td>
                          <td>{{ $item->updated_at }}</td>
                          <td style="max-width:40px">
                              <a type="button" class="btn btn-square btn-success {{ strpos(Auth::user()->menuroles, 'admin') ? '' : 'disabled' }}" href="/admin/category/edit/{{ $item->id }}"><i class="fas fa-edit"></i></a>
                          </td>
                        <td style="max-width:40px">
                          <form action="{{ route('admin.category.delete', $item->id) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-square btn-danger" {{ strpos(Auth::user()->menuroles, 'admin') ? '' : 'disabled' }} type="button" data-toggle="modal" data-target="#dangerModal-{{$item->id}}"><i class="fas fa-trash-alt"></i></button>
                            @if(strpos(Auth::user()->menuroles, 'admin'))
                              <div class="modal fade" id="dangerModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-danger" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title">Save Blog</h4>
                                      <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    </div>
                                    <div class="modal-body">
                                      <p>Are you sure you want to save changes?</p>
                                        {{$item->id}}
                                    </div>
                                    <div class="modal-footer">
                                      <button class="btn btn-secondary btn-square col-md-2" type="button" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-square btn-danger col-md-2" value="upload">Save</button>
                                    </div>
                                  </div>
                                  <!-- /.modal-content-->
                                </div>
                                <!-- /.modal-dialog-->
                              </div>
                            @endif
                          </form>
                        </td>
                        </tr>
                        @endforeach
                    @endif

                  </tbody>
                </table>
              </div>
              <div class="card-footer">
                {{ $data->links() }}
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('javascript')

@endsection