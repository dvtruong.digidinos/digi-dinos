@extends('dashboard.category')

@section('content')
<div class="container-fluid">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card">
          <form id="create-edit" action="{{ !Request::is('admin/category/create') ? route("admin.category.update", $id) : route('admin.category.create') }}" method="POST">
            @csrf
            @if (isset($dataEdit))
            @method('PATCH')
            @endif
            <div class="card-header"><strong>Create Category</strong></div>
            @if(count($errors) > 0)
            <div class="alert alert-danger" role="alert">
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </div>
            @endif
            <div class="card-body was-validated">
              <div class="form-group">
                <label class="form-col-form-label" for="inputSuccess2">Name</label>
                <input class="form-control col-sm-8  is-valid" id="inputSuccess2" name="name" type="text" required="" value="{{ isset($dataEdit) && $dataEdit->name ? $dataEdit->name : '' }}">
              </div>
              <div class="form-group">
                <label class="form-col-form-label" for="inputError2">Type</label>
                <select class="form-control col-sm-8 " name="type" id="ccmonth" required>
                    <option {{ isset($dataEdit) && $dataEdit->type == 0 ? 'selected' : ''}} value="0">News</option>
                    <option {{ isset($dataEdit) && $dataEdit->type == 1 ? 'selected' : ''}} value="1">Blogs</option>
                    <option {{ isset($dataEdit) && $dataEdit->type == 2 ? 'selected' : ''}} value="2">Magazine</option>
                </select>
              </div>

              <div class="form-group">
                <label class="form-col-form-label" for="inputError2">Descriptions</label>
                <div class="">
                  <textarea class="form-control" id="textarea-input" name="description" rows="5" placeholder="Descriptions.." required="">{{  isset($dataEdit) ? $dataEdit->description : '' }}</textarea>
                </div>
              </div>

              <div class="form-group">
                <div class="container-fluid">
                  <div class="row">
                  <button class="btn btn-square btn-success btn-save " type="button" >Save</button>
                    <div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-success" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title">Save Blog</h4>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                          </div>
                          <div class="modal-body">
                            <p>Are you sure you want to save changes?</p>
                          </div>
                          <div class="modal-footer">
                            <button class="btn btn-secondary btn-square col-md-2" type="button" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-square btn-success col-md-2" value="upload">Save</button>
                          </div>
                        </div>
                        <!-- /.modal-content-->
                      </div>
                      <!-- /.modal-dialog-->
                    </div>
                    <a type="button" class="btn btn-square btn-danger btn-save" href="/admin/category/list">Cancel</a>
                  </div>
                </div>
              </div>
            </div>
          </form>
          </div>
        </div>
    </div>
  </div>
</div>
@endsection