@extends('dashboard.blog')

@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">Search Blog</div>
          <div class="card-body">
            <form class="form-horizontal" action="{{ route('admin.blog.search') }}" method="get" enctype="multipart/form-data">

              <div class="form-group row">
                <label class="col-md-1 col-form-label" for="text-input">Title</label>
                <div class="col-md-11">
                  <input class="form-control" id="text-input" type="text" name="title" placeholder="Title">
                </div>
              </div>
              <div class="collapse" id="collapseExample">
                {{-- content --}}
                <div class="form-group row">
                    <label class="col-md-1 col-form-label" for="text-input">Authord</label>
                    <div class="col-md-11">
                      <select name="users_id"class="form-control" id="ccmonth">
                        <option value=""></option>
                        @if(isset($auth))
                          @foreach ($auth as $el)
                            <option value="{{$el->id}}">{{$el->name}}</option>
                          @endforeach
                        @endif
                      </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-md-1 col-form-label" for="text-input">Status</label>
                    <div class="col-md-11">
                       <div class="form-check form-check-inline mr-2">
                        <input class="form-check-input" id="inline-radio1" type="radio" value="0" name="status">
                        <label class="form-check-label" for="inline-radio1">Public</label>
                      </div>
                      <div class="form-check form-check-inline mr-2">
                        <input class="form-check-input" id="inline-radio2" type="radio" value="1" name="status">
                        <label class="form-check-label" for="inline-radio2">Private</label>
                      </div>
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <label class="col-md-1 col-form-label" for="text-input">Category</label>
                    <div class="col-md-11">
                      <select name="category" id="category" name="users_id"class="form-control"  >
                      <option value="">Select Category</option>
                      @if(isset($category))
                          @foreach ($category as $el)
                            <option  value="{{ $el->id }}">{{ $el->name }}</option>
                          @endforeach
                        @endif
                      </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-md-1 col-form-label" for="date-input">Start At</label>
                    <div class="col-md-11">
                      <input name="start_at" class="form-control" id="date-input" type="date" name="date-input" placeholder="">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-md-1 col-form-label" for="date-input">End At</label>
                    <div class="col-md-11">
                      <input name="end_at" class="form-control" id="date-input" type="date" name="date-input" placeholder="">
                    </div>
                  </div>

                </div>
                <button class="btn btn-square btn-dark" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="fas fa-plus"></i><span class="search-expand">&nbsp;Expand</span></a></button>
                <div class="form-group row">
                  <div class="btn-search">
                    <button type="submit" class="btn btn-square btn-success">Search &nbsp;<i class="fas fa-search"></i></button>
                  </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
          <div class="card">
              <div class="card-header">
                Blog List
              </div>
              <div class="card-body">
                  <table class="table table-responsive-sm">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Title</th>
                      <th>Image</th>
                      <th>Category</th>
                      <th>Status</th>
                      <th>Descriptions</th>
                      <th>Created At</th>
                      <th>Updated At</th>
                      <th>Edit</th>
                      <th>Delete</th>
                    </tr>
                  </thead>
                  <tbody>
                    @if (isset($data))
                        @foreach ($data as $item)
                        <tr>
                          <td>{{ $item->id }}</td>
                          <td>{{ $item->title }}</td>
                          <td>
                            <img class="blog-image" src="{{ url($item->head_image ? $item->head_image : '' ) }}" alt=""></td>
                          <td>
                            @foreach ($category as $el)
                              @if ($el->id == $item->category_id)
                                  {{ $el->type == 0 ? 'News' : ($el->type == 1 ? 'Blog' : 'Magazine' )}}
                              @endif
                            @endforeach
                          </td>
                        <td><span class="badge badge-{{ $item->status == 0 ? 'success' : 'warning'}}">{{ $item->status == 0 ? 'Public' : 'Private' }}</span></td>
                          <td>{{ $item->description }}</td>
                          <td>{{ $item->created_at }}</td>
                          <td>{{ $item->updated_at }}</td>
                          <td>
                              <a type="button" class="btn btn-square btn-success" href="/admin/blog/edit/{{ $item->id }}"><i class="fas fa-edit"></i></a>
                          </td>
                        <td>
                          <form action="{{ route('admin.blog.delete', $item->id) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-square btn-danger " type="button" data-toggle="modal" data-target="#dangerModal-{{$item->id}}"><i class="fas fa-trash-alt"></i></button>
                              <div class="modal fade" id="dangerModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-danger" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title">Save Blog</h4>
                                      <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    </div>
                                    <div class="modal-body">
                                      <p>Are you sure you want to save changes?</p>
                                    </div>
                                    <div class="modal-footer">
                                      <button class="btn btn-secondary btn-square col-md-2" type="button" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-square btn-danger col-md-2" value="upload">Save</button>
                                    </div>
                                  </div>
                                  <!-- /.modal-content-->
                                </div>
                                <!-- /.modal-dialog-->
                              </div>
                          </form>
                        </td>
                        </tr>
                        @endforeach
                    @endif
                  </tbody>
                </table>
              </div>
              <div class="card-footer">
                {{ $data->links() }}
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('javascript')

@endsection