@extends('dashboard.blog')

@section('content')
<div class="container-fiuld">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <form id="create-edit" action="{{ Request::is('admin/blog/create') ? route('admin.blog.create') : route("admin.blog.update", $id) }}" method="POST" enctype="multipart/form-data">
          @csrf
          @if (isset($data))
            @method('PATCH')
          @endif
          <div class="">
            <div class="card">
              <div class="card-header"><strong>Create Blogs</strong></div>
                @if(count($errors) > 0)
                <div class="alert alert-danger" role="alert">
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </div>
              @endif
              <div class="card-body was-validated ">
                <div class="form-group">
                  <label class="form-col-form-label" for="inputSuccess2">Title</label>
                  <input class="form-control col-sm-8 " id="inputSuccess2" name="title" type="text" required value="{{ isset($data) && $data->title ? $data->title : '' }}">
                </div>
                <div class="form-group">
                  @if(isset($data))
                  <label class="form-col-form-label" for="inputError2">Authord</label>
                  <input readonly class="form-control col-sm-8 " type="text" value="{{ isset($data) && $data->user->name ? $data->user->name : ''}}">
                  <input readonly class="form-control col-sm-8 " name="users_id" type="hidden" value="{{isset($data) && $data->user->id ? $data->user->id : '' }}">
                  @else
                  <div class="form-group">
                    <label class="form-col-form-label" for="inputError2">Authord</label>
                    <select name="users_id"class="form-control col-sm-8 " id="ccmonth" required="">
                      @if(isset($auth))
                        @foreach ($auth as $el)
                          <option value="{{$el->id}}">{{$el->name}}</option>
                        @endforeach
                      @endif
                    </select>
                  </div>
                  @endif
                </div>

                <div class="form-group row">
                  <label class="col-md-1 col-form-label" for="">Status</label>
                  <div class="col-md-11  col-form-label">
                    <div class="form-check form-check-inline mr-2">
                      <input class="form-check-input" id="" type="radio" {{ isset($data) && $data->status == 0 ? 'checked' : ''}} value="0" name="status">
                      <span class="" for="">Public</span>
                    </div>
                    <div class="form-check form-check-inline mr-2">
                      <input class="form-check-input"  {{isset($data) && $data->status == 1 ? 'checked' : ''}} type="radio" value="1" name="status">
                      <span class="" for="">Private</span>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="form-col-form-label" for="inputError2">Category</label>
                  <select class="form-control col-sm-8 " name="category" id="ccmonth" required="">
                    @foreach ($category as $el)
                      <option {{ isset($data) && $data->category_id == $el->id ? 'selected' : '' }} value="{{ $el->id }}">{{ $el->name }}</option>
                    @endforeach
                  </select>
                </div>

                <div class="form-group">
                  <label class="form-col-form-label" for="inputError2">Image</label>
                  <input type="file" name="filename" class="file" accept="image/*">
                    <div class="input-group my-3">
                      <input type="text" class="form-control col-sm-3" disabled placeholder="Upload File" id="file">
                      <div class="input-group-append" >
                        <button type="button" class="browse btn btn-primary" >Browse...</button>
                      </div>
                    </div>
                    <div class="ml-2 col-sm-6">
                      <img src="" id="preview" class="img-thumbnail img-thumbnail-disabled">
                      @if(isset($data) && $data->head_image)
                      <img src=" {{ url($data->head_image) }}" id="preview" class="img-thumbnail is-edit">
                      @else
                      <img src=" {{ asset('assets/img/no-image.png') }}" id="preview" class="img-thumbnail no-image">
                      @endif
                    </div>
                  </div>
                <div class="form-group">
                  <label class="form-col-form-label" for="inputError2">Descriptions</label>
                  <div class="">
                    <textarea class="form-control" id="textarea-input" name="description" rows="5" placeholder="Descriptions.." required="">{{ isset($data) && $data->description ? $data->description : '' }}</textarea>
                  </div>
                </div>

                <div class="form-group">
                  <label class="form-col-form-label" for="inputError2">Contents</label>
                  <div class="">
                    <textarea id="editor1" class="form-control" id="textarea-input" name="content" rows="10"  placeholder="Contents.." required="">{{ isset($data) && $data->content ? $data->content : '' }}</textarea>
                        <script>
                          window.onload = function() {
                              CKEDITOR.replace( 'editor1', {
                                extraPlugins: 'codesnippet',
                                codeSnippet_theme: 'monokai'
                              });
                          };
                        </script>
                  </div>
                </div>
                <div class="form-group">
                  <div class="container-fluid">
                    <div class="row">
                      <button class="btn btn-square btn-success btn-save" type="button" >Save</button>
                      <div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-success" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h4 class="modal-title">Save Blog</h4>
                              <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body">
                              <p>Are you sure you want to save changes?</p>
                            </div>
                            <div class="modal-footer">
                              <button class="btn btn-secondary btn-square col-md-2" type="button" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-square btn-success col-md-2" value="upload">Save</button>
                            </div>
                          </div>
                          <!-- /.modal-content-->
                        </div>
                        <!-- /.modal-dialog-->
                      </div>
                      <a href="admin/blog/list" type="button" class="btn btn-square btn-danger btn-save">Cancel</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection