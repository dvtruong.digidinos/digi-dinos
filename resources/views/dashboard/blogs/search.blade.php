@extends('dashboard.blog')

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-8 col-md-10 col-lg-12 col-xl-12">
      <div class="card">
          <div class="card-header">Result</div>
          <div class="card-body">
              <table class="table table-responsive-sm">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Title</th>
                  <th>Image</th>
                  <th>Category</th>
                  <th>Status</th>
                  <th>Descriptions</th>
                  <th>Created At</th>
                  <th>Updated At</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>
                @if (isset($data))
                    @foreach ($data as $item)
                    <tr>
                      <td>{{ $item->id }}</td>
                      <td>{{ $item->title }}</td>
                      <td>{{ $item->head_image }}</td>
                      <td>{{ $item->category_id}}</td>
                      <td><span class="badge badge-success">{{ $item->status }}</span></td>
                      <td>{{ $item->description }}</td>
                      <td>{{ $item->created_at }}</td>
                      <td>{{ $item->updated_at }}</td>
                      <td>
                          <a type="button" class="btn btn-square btn-block btn-success" href="admin/blog/edit/{{ $item->id }}"><i class="fas fa-edit"></i></a>
                      </td>
                    <td>
                      <form action="{{ route('admin.blog.delete', $item->id) }}" method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-square btn-block btn-danger" type="submit"><i class="fas fa-trash-alt"></i></button>
                      </form>
                    </td>
                    </tr>
                    @endforeach
                @endif

              </tbody>
            </table>
          </div>
      </div>
    </div>
  </div>
</div>
@endsection
