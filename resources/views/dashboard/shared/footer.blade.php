<footer class="c-footer">
  <div><a href="https://digidinos.com/">Copyright</a> &copy; 2018 Digi Dinos. All rights reserved.</div>
  <div class="ml-auto">Powered by&nbsp;<a href="https://digidinos.com/">Digi Dinos</a></div>
</footer>