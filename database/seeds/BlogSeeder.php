<?php

use Illuminate\Database\Seeder;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('posts')->insert([
            [
            'title'=> 'Số phận pháp lý của ông Nguyễn Bắc Son khi khai nhận hối lộ',
            'users_id' => '1',
            'status' => '1',
            'category_id' => '1',
            'description' => 'HÀ NỘI - Luật sư Vũ Quang Đức cho rằng nguồn gốc số tiền ông Nguyễn Bắc Son dàn xếp nộp lại trong hôm nay sẽ "quyết định" ông có được hưởng tình tiết giảm nhẹ hay không.',
            'head_content' => 'TAND Hà Nội dừng phiên tòa một ngày để cựu bộ trưởng Thông tin và Truyền thông Nguyễn Bắc Son gặp gia đình bàn cách khắc phục hậu quả của hành vi nhận hối lộ 3 triệu USD (hơn 66,5 tỷ đồng). 

                                Các cơ quan tố tụng cáo buộc quá trình thực hiện dự án MobiFone mua 95% cổ phần AVG, chủ tịch AVG Phạm Nhật Vũ đã nhiều lần liên hệ điện thoại với ông Son để trao đổi công việc, hối thúc thực hiện nhanh việc mua, bán. Khi hoàn thành dự án, ông Vũ đưa ông Son 3 triệu USD (hơn 66 tỷ đồng). Toàn bộ số tiền, ông đưa cho con gái Nguyễn Thị Thu Huyền, dặn "không được gửi tiết kiệm, còn đầu tư vào đâu thì tùy".

                                Theo cáo trạng, ông Son nhận thức số tiền trên là bất hợp pháp nên nhiều lần viết đơn xin nộp lại, tuy nhiên không nhận được sự phối hợp của gia đình. Tại phiên toà sáng 17/12, ông Son phủ nhận cáo buộc nhận hối lộ, một mực khẳng định: "Tôi không nhận bất cứ khoản tiền nào". Chỉ hơn hai tiếng sau, qua giờ nghỉ trưa, ông thay đổi lời khai, nói có nhận 3 triệu USD, đã dùng chi tiêu cá nhân chứ không đưa con gái. 

                                Hôm sau, ông Son bất ngờ xin HĐXX cho được gặp gia đình để lo sớm khắc phục hậu quả vụ án. Dù vậy, ông khai không nhớ đã "tiêu 3 triệu USD nhận hối lộ vào việc gì".

                                Ngày 19/12, phân tích các động thái này của ông Son, luật sư Vũ Quang Đức (Đoàn Luật sư TP HCM) cho biết trong quá trình tố tụng, một người có thể khai nhiều lần dù ở giai đoạn điều tra hay tại phiên toà. Những lời khai có thể không giống nhau, thậm chí mâu thuẫn. Vì thế, việc thay đổi lời khai của ông Son không vi phạm tố tụng.

                                Tuy nhiên, nếu lời khai cuối cùng được xác nhận là không đúng sự thật, ông có thể bị mất tình tiết giảm nhẹ "thành khẩn khai báo" và "ăn năn hối cải". Theo nguyên tắc tố tụng, nếu bị cáo có nhiều lời khai khác nhau, HĐXX có quyền chọn lời khai để làm chứng cứ.',
            'content' => 'Cũng theo luật sư, cô Huyền thừa nhận hay phủ nhận có cầm 3 triệu USD thì lúc này việc nộp tiền sẽ khác nhau về bản chất. Giả thiết thứ nhất, ông Son giữ nguyên lời khai đã đưa tiền cho con gái và các cơ quan tố tụng chứng minh được điều này là đúng, kể cả cô này không thừa nhận, ông Son vẫn bị khép tội Nhận hối lộ. Theo điều 354 Bộ luật Hình sự, với số tiền 3 triệu USD, cựu bộ trưởng phải đối mặt mức án cao nhất là tử hình.

                            Bên cạnh đó, con gái ông Son có thể đối mặt việc bị truy cứu trách nhiệm hình sự về tội Chứa chấp hoặc tiêu thụ tài sản do người khác phạm tội mà có (điều 323 Bộ luật Hình sự 2015). Mức hình phạt cao nhất của tội này lên tới 15 năm tù nếu tài sản, tang vật phạm pháp trị giá một tỷ đồng trở lên.

                            Trường hợp nhận tiền từ cha nhưng không biết nguồn gốc, cô Huyền có thể không bị truy cứu trách nhiệm hình sự. Nhưng ở trường hợp này, cô vẫn phải nộp lại 3 triệu USD theo hình thức thu hồi.

                            Nếu cơ quan điều tra không thu đủ, con ông Son phải nộp bù. Việc này chỉ được coi là "nộp lại tài sản phạm tội mà có", không phải là "khắc phục hậu quả". Ông Son vì thế không được hưởng tình tiết giảm nhẹ "khắc phục hậu quả".

                            Giả thiết thứ hai, ông Son thừa nhận hành vi nhận hối lộ 3 triệu USD song khai không đưa cho con gái mà bản thân tiêu gì cũng không nhớ, lại liên hệ gia đình khắc phục số tiền này thì "số phận pháp lý" lại theo hướng khác.

                            Theo luật sư, ông dùng tài sản riêng của gia đình để nộp lại và khoản này không liên quan tiền nhận hối lộ thì sẽ được coi là "khắc phục hậu quả".

                            Tiền khắc phục hậu quả được nộp ở giai đoạn xét xử sơ thẩm, việc xem xét giảm án tử hình có thể được ghi nhận khi tòa tuyên án sơ thẩm. Nộp sau khi có án sơ thẩm, việc giảm án được xem xét ở cấp phúc thẩm. Khi bản án phúc thẩm có hiệu lực, việc nộp tiền và xem xét ân giảm án sẽ được thực hiện ở quá trình thi hành án.

                            Theo điều 40 Bộ luật Hình sự 2015: Người bị kết án tử hình về tội Tham ô tài sản, Nhận hối lộ mà sau khi bị kết án đã chủ động nộp lại ít nhất 3/4 tài sản phạm tội và hợp tác tích cực với cơ quan chức năng trong việc phát hiện, điều tra, xử lý tội phạm hoặc lập công lớn thì không bị thi hành án tử hình.',
            'foot_content' => 'Phiên sơ thẩm xét xử 14 bị cáo có sai phạm tại dự án MobiFone mua AVG diễn ra tại TAND Hà Nội từ ngày 16 đến 31/12. Trong các bị cáo, hai cựu bộ trưởng Thông tin và Truyền thông là Trương Minh Tuấn, Nguyễn Bắc Son bị truy tố về hai tội Vi phạm các quy định về quản lý đầu tư công gây hậu quả nghiêm trọng (điều 220 Bộ luật Hình sự), Nhận hối lộ (Điều 354) với khung hình phạt 20 năm tù, chung thân hoặc tử hình.

                                Cựu chủ tịch AVG Phạm Nhật Vũ bị xét xử tội Đưa hối lộ (điều 364), đối mặt mức hình phạt 12-20 năm tù.'
            ],
            [
            'title'=> 'Số phận pháp lý của ông Nguyễn Bắc Son khi khai nhận hối lộ',
            'users_id' => '1',
            'status' => '1',
            'category_id' => '1',
            'description' => 'HÀ NỘI - Luật sư Vũ Quang Đức cho rằng nguồn gốc số tiền ông Nguyễn Bắc Son dàn xếp nộp lại trong hôm nay sẽ "quyết định" ông có được hưởng tình tiết giảm nhẹ hay không.',
            'head_content' => 'TAND Hà Nội dừng phiên tòa một ngày để cựu bộ trưởng Thông tin và Truyền thông Nguyễn Bắc Son gặp gia đình bàn cách khắc phục hậu quả của hành vi nhận hối lộ 3 triệu USD (hơn 66,5 tỷ đồng). 

                                Các cơ quan tố tụng cáo buộc quá trình thực hiện dự án MobiFone mua 95% cổ phần AVG, chủ tịch AVG Phạm Nhật Vũ đã nhiều lần liên hệ điện thoại với ông Son để trao đổi công việc, hối thúc thực hiện nhanh việc mua, bán. Khi hoàn thành dự án, ông Vũ đưa ông Son 3 triệu USD (hơn 66 tỷ đồng). Toàn bộ số tiền, ông đưa cho con gái Nguyễn Thị Thu Huyền, dặn "không được gửi tiết kiệm, còn đầu tư vào đâu thì tùy".

                                Theo cáo trạng, ông Son nhận thức số tiền trên là bất hợp pháp nên nhiều lần viết đơn xin nộp lại, tuy nhiên không nhận được sự phối hợp của gia đình. Tại phiên toà sáng 17/12, ông Son phủ nhận cáo buộc nhận hối lộ, một mực khẳng định: "Tôi không nhận bất cứ khoản tiền nào". Chỉ hơn hai tiếng sau, qua giờ nghỉ trưa, ông thay đổi lời khai, nói có nhận 3 triệu USD, đã dùng chi tiêu cá nhân chứ không đưa con gái. 

                                Hôm sau, ông Son bất ngờ xin HĐXX cho được gặp gia đình để lo sớm khắc phục hậu quả vụ án. Dù vậy, ông khai không nhớ đã "tiêu 3 triệu USD nhận hối lộ vào việc gì".

                                Ngày 19/12, phân tích các động thái này của ông Son, luật sư Vũ Quang Đức (Đoàn Luật sư TP HCM) cho biết trong quá trình tố tụng, một người có thể khai nhiều lần dù ở giai đoạn điều tra hay tại phiên toà. Những lời khai có thể không giống nhau, thậm chí mâu thuẫn. Vì thế, việc thay đổi lời khai của ông Son không vi phạm tố tụng.

                                Tuy nhiên, nếu lời khai cuối cùng được xác nhận là không đúng sự thật, ông có thể bị mất tình tiết giảm nhẹ "thành khẩn khai báo" và "ăn năn hối cải". Theo nguyên tắc tố tụng, nếu bị cáo có nhiều lời khai khác nhau, HĐXX có quyền chọn lời khai để làm chứng cứ.',
            'content' => 'Cũng theo luật sư, cô Huyền thừa nhận hay phủ nhận có cầm 3 triệu USD thì lúc này việc nộp tiền sẽ khác nhau về bản chất. Giả thiết thứ nhất, ông Son giữ nguyên lời khai đã đưa tiền cho con gái và các cơ quan tố tụng chứng minh được điều này là đúng, kể cả cô này không thừa nhận, ông Son vẫn bị khép tội Nhận hối lộ. Theo điều 354 Bộ luật Hình sự, với số tiền 3 triệu USD, cựu bộ trưởng phải đối mặt mức án cao nhất là tử hình.

                            Bên cạnh đó, con gái ông Son có thể đối mặt việc bị truy cứu trách nhiệm hình sự về tội Chứa chấp hoặc tiêu thụ tài sản do người khác phạm tội mà có (điều 323 Bộ luật Hình sự 2015). Mức hình phạt cao nhất của tội này lên tới 15 năm tù nếu tài sản, tang vật phạm pháp trị giá một tỷ đồng trở lên.

                            Trường hợp nhận tiền từ cha nhưng không biết nguồn gốc, cô Huyền có thể không bị truy cứu trách nhiệm hình sự. Nhưng ở trường hợp này, cô vẫn phải nộp lại 3 triệu USD theo hình thức thu hồi.

                            Nếu cơ quan điều tra không thu đủ, con ông Son phải nộp bù. Việc này chỉ được coi là "nộp lại tài sản phạm tội mà có", không phải là "khắc phục hậu quả". Ông Son vì thế không được hưởng tình tiết giảm nhẹ "khắc phục hậu quả".

                            Giả thiết thứ hai, ông Son thừa nhận hành vi nhận hối lộ 3 triệu USD song khai không đưa cho con gái mà bản thân tiêu gì cũng không nhớ, lại liên hệ gia đình khắc phục số tiền này thì "số phận pháp lý" lại theo hướng khác.

                            Theo luật sư, ông dùng tài sản riêng của gia đình để nộp lại và khoản này không liên quan tiền nhận hối lộ thì sẽ được coi là "khắc phục hậu quả".

                            Tiền khắc phục hậu quả được nộp ở giai đoạn xét xử sơ thẩm, việc xem xét giảm án tử hình có thể được ghi nhận khi tòa tuyên án sơ thẩm. Nộp sau khi có án sơ thẩm, việc giảm án được xem xét ở cấp phúc thẩm. Khi bản án phúc thẩm có hiệu lực, việc nộp tiền và xem xét ân giảm án sẽ được thực hiện ở quá trình thi hành án.

                            Theo điều 40 Bộ luật Hình sự 2015: Người bị kết án tử hình về tội Tham ô tài sản, Nhận hối lộ mà sau khi bị kết án đã chủ động nộp lại ít nhất 3/4 tài sản phạm tội và hợp tác tích cực với cơ quan chức năng trong việc phát hiện, điều tra, xử lý tội phạm hoặc lập công lớn thì không bị thi hành án tử hình.',
            'foot_content' => 'Phiên sơ thẩm xét xử 14 bị cáo có sai phạm tại dự án MobiFone mua AVG diễn ra tại TAND Hà Nội từ ngày 16 đến 31/12. Trong các bị cáo, hai cựu bộ trưởng Thông tin và Truyền thông là Trương Minh Tuấn, Nguyễn Bắc Son bị truy tố về hai tội Vi phạm các quy định về quản lý đầu tư công gây hậu quả nghiêm trọng (điều 220 Bộ luật Hình sự), Nhận hối lộ (Điều 354) với khung hình phạt 20 năm tù, chung thân hoặc tử hình.

                                Cựu chủ tịch AVG Phạm Nhật Vũ bị xét xử tội Đưa hối lộ (điều 364), đối mặt mức hình phạt 12-20 năm tù.'
            ],
            [
            'title'=> 'Số phận pháp lý của ông Nguyễn Bắc Son khi khai nhận hối lộ',
            'users_id' => '1',
            'status' => '1',
            'category_id' => '1',
            'description' => 'HÀ NỘI - Luật sư Vũ Quang Đức cho rằng nguồn gốc số tiền ông Nguyễn Bắc Son dàn xếp nộp lại trong hôm nay sẽ "quyết định" ông có được hưởng tình tiết giảm nhẹ hay không.',
            'head_content' => 'TAND Hà Nội dừng phiên tòa một ngày để cựu bộ trưởng Thông tin và Truyền thông Nguyễn Bắc Son gặp gia đình bàn cách khắc phục hậu quả của hành vi nhận hối lộ 3 triệu USD (hơn 66,5 tỷ đồng). 

                                Các cơ quan tố tụng cáo buộc quá trình thực hiện dự án MobiFone mua 95% cổ phần AVG, chủ tịch AVG Phạm Nhật Vũ đã nhiều lần liên hệ điện thoại với ông Son để trao đổi công việc, hối thúc thực hiện nhanh việc mua, bán. Khi hoàn thành dự án, ông Vũ đưa ông Son 3 triệu USD (hơn 66 tỷ đồng). Toàn bộ số tiền, ông đưa cho con gái Nguyễn Thị Thu Huyền, dặn "không được gửi tiết kiệm, còn đầu tư vào đâu thì tùy".

                                Theo cáo trạng, ông Son nhận thức số tiền trên là bất hợp pháp nên nhiều lần viết đơn xin nộp lại, tuy nhiên không nhận được sự phối hợp của gia đình. Tại phiên toà sáng 17/12, ông Son phủ nhận cáo buộc nhận hối lộ, một mực khẳng định: "Tôi không nhận bất cứ khoản tiền nào". Chỉ hơn hai tiếng sau, qua giờ nghỉ trưa, ông thay đổi lời khai, nói có nhận 3 triệu USD, đã dùng chi tiêu cá nhân chứ không đưa con gái. 

                                Hôm sau, ông Son bất ngờ xin HĐXX cho được gặp gia đình để lo sớm khắc phục hậu quả vụ án. Dù vậy, ông khai không nhớ đã "tiêu 3 triệu USD nhận hối lộ vào việc gì".

                                Ngày 19/12, phân tích các động thái này của ông Son, luật sư Vũ Quang Đức (Đoàn Luật sư TP HCM) cho biết trong quá trình tố tụng, một người có thể khai nhiều lần dù ở giai đoạn điều tra hay tại phiên toà. Những lời khai có thể không giống nhau, thậm chí mâu thuẫn. Vì thế, việc thay đổi lời khai của ông Son không vi phạm tố tụng.

                                Tuy nhiên, nếu lời khai cuối cùng được xác nhận là không đúng sự thật, ông có thể bị mất tình tiết giảm nhẹ "thành khẩn khai báo" và "ăn năn hối cải". Theo nguyên tắc tố tụng, nếu bị cáo có nhiều lời khai khác nhau, HĐXX có quyền chọn lời khai để làm chứng cứ.',
            'content' => 'Cũng theo luật sư, cô Huyền thừa nhận hay phủ nhận có cầm 3 triệu USD thì lúc này việc nộp tiền sẽ khác nhau về bản chất. Giả thiết thứ nhất, ông Son giữ nguyên lời khai đã đưa tiền cho con gái và các cơ quan tố tụng chứng minh được điều này là đúng, kể cả cô này không thừa nhận, ông Son vẫn bị khép tội Nhận hối lộ. Theo điều 354 Bộ luật Hình sự, với số tiền 3 triệu USD, cựu bộ trưởng phải đối mặt mức án cao nhất là tử hình.

                            Bên cạnh đó, con gái ông Son có thể đối mặt việc bị truy cứu trách nhiệm hình sự về tội Chứa chấp hoặc tiêu thụ tài sản do người khác phạm tội mà có (điều 323 Bộ luật Hình sự 2015). Mức hình phạt cao nhất của tội này lên tới 15 năm tù nếu tài sản, tang vật phạm pháp trị giá một tỷ đồng trở lên.

                            Trường hợp nhận tiền từ cha nhưng không biết nguồn gốc, cô Huyền có thể không bị truy cứu trách nhiệm hình sự. Nhưng ở trường hợp này, cô vẫn phải nộp lại 3 triệu USD theo hình thức thu hồi.

                            Nếu cơ quan điều tra không thu đủ, con ông Son phải nộp bù. Việc này chỉ được coi là "nộp lại tài sản phạm tội mà có", không phải là "khắc phục hậu quả". Ông Son vì thế không được hưởng tình tiết giảm nhẹ "khắc phục hậu quả".

                            Giả thiết thứ hai, ông Son thừa nhận hành vi nhận hối lộ 3 triệu USD song khai không đưa cho con gái mà bản thân tiêu gì cũng không nhớ, lại liên hệ gia đình khắc phục số tiền này thì "số phận pháp lý" lại theo hướng khác.

                            Theo luật sư, ông dùng tài sản riêng của gia đình để nộp lại và khoản này không liên quan tiền nhận hối lộ thì sẽ được coi là "khắc phục hậu quả".

                            Tiền khắc phục hậu quả được nộp ở giai đoạn xét xử sơ thẩm, việc xem xét giảm án tử hình có thể được ghi nhận khi tòa tuyên án sơ thẩm. Nộp sau khi có án sơ thẩm, việc giảm án được xem xét ở cấp phúc thẩm. Khi bản án phúc thẩm có hiệu lực, việc nộp tiền và xem xét ân giảm án sẽ được thực hiện ở quá trình thi hành án.

                            Theo điều 40 Bộ luật Hình sự 2015: Người bị kết án tử hình về tội Tham ô tài sản, Nhận hối lộ mà sau khi bị kết án đã chủ động nộp lại ít nhất 3/4 tài sản phạm tội và hợp tác tích cực với cơ quan chức năng trong việc phát hiện, điều tra, xử lý tội phạm hoặc lập công lớn thì không bị thi hành án tử hình.',
            'foot_content' => 'Phiên sơ thẩm xét xử 14 bị cáo có sai phạm tại dự án MobiFone mua AVG diễn ra tại TAND Hà Nội từ ngày 16 đến 31/12. Trong các bị cáo, hai cựu bộ trưởng Thông tin và Truyền thông là Trương Minh Tuấn, Nguyễn Bắc Son bị truy tố về hai tội Vi phạm các quy định về quản lý đầu tư công gây hậu quả nghiêm trọng (điều 220 Bộ luật Hình sự), Nhận hối lộ (Điều 354) với khung hình phạt 20 năm tù, chung thân hoặc tử hình.

                                Cựu chủ tịch AVG Phạm Nhật Vũ bị xét xử tội Đưa hối lộ (điều 364), đối mặt mức hình phạt 12-20 năm tù.'
            ],
            [
            'title'=> 'Số phận pháp lý của ông Nguyễn Bắc Son khi khai nhận hối lộ',
            'users_id' => '1',
            'status' => '1',
            'category_id' => '2',
            'description' => 'HÀ NỘI - Luật sư Vũ Quang Đức cho rằng nguồn gốc số tiền ông Nguyễn Bắc Son dàn xếp nộp lại trong hôm nay sẽ "quyết định" ông có được hưởng tình tiết giảm nhẹ hay không.',
            'head_content' => 'TAND Hà Nội dừng phiên tòa một ngày để cựu bộ trưởng Thông tin và Truyền thông Nguyễn Bắc Son gặp gia đình bàn cách khắc phục hậu quả của hành vi nhận hối lộ 3 triệu USD (hơn 66,5 tỷ đồng). 

                                Các cơ quan tố tụng cáo buộc quá trình thực hiện dự án MobiFone mua 95% cổ phần AVG, chủ tịch AVG Phạm Nhật Vũ đã nhiều lần liên hệ điện thoại với ông Son để trao đổi công việc, hối thúc thực hiện nhanh việc mua, bán. Khi hoàn thành dự án, ông Vũ đưa ông Son 3 triệu USD (hơn 66 tỷ đồng). Toàn bộ số tiền, ông đưa cho con gái Nguyễn Thị Thu Huyền, dặn "không được gửi tiết kiệm, còn đầu tư vào đâu thì tùy".

                                Theo cáo trạng, ông Son nhận thức số tiền trên là bất hợp pháp nên nhiều lần viết đơn xin nộp lại, tuy nhiên không nhận được sự phối hợp của gia đình. Tại phiên toà sáng 17/12, ông Son phủ nhận cáo buộc nhận hối lộ, một mực khẳng định: "Tôi không nhận bất cứ khoản tiền nào". Chỉ hơn hai tiếng sau, qua giờ nghỉ trưa, ông thay đổi lời khai, nói có nhận 3 triệu USD, đã dùng chi tiêu cá nhân chứ không đưa con gái. 

                                Hôm sau, ông Son bất ngờ xin HĐXX cho được gặp gia đình để lo sớm khắc phục hậu quả vụ án. Dù vậy, ông khai không nhớ đã "tiêu 3 triệu USD nhận hối lộ vào việc gì".

                                Ngày 19/12, phân tích các động thái này của ông Son, luật sư Vũ Quang Đức (Đoàn Luật sư TP HCM) cho biết trong quá trình tố tụng, một người có thể khai nhiều lần dù ở giai đoạn điều tra hay tại phiên toà. Những lời khai có thể không giống nhau, thậm chí mâu thuẫn. Vì thế, việc thay đổi lời khai của ông Son không vi phạm tố tụng.

                                Tuy nhiên, nếu lời khai cuối cùng được xác nhận là không đúng sự thật, ông có thể bị mất tình tiết giảm nhẹ "thành khẩn khai báo" và "ăn năn hối cải". Theo nguyên tắc tố tụng, nếu bị cáo có nhiều lời khai khác nhau, HĐXX có quyền chọn lời khai để làm chứng cứ.',
            'content' => 'Cũng theo luật sư, cô Huyền thừa nhận hay phủ nhận có cầm 3 triệu USD thì lúc này việc nộp tiền sẽ khác nhau về bản chất. Giả thiết thứ nhất, ông Son giữ nguyên lời khai đã đưa tiền cho con gái và các cơ quan tố tụng chứng minh được điều này là đúng, kể cả cô này không thừa nhận, ông Son vẫn bị khép tội Nhận hối lộ. Theo điều 354 Bộ luật Hình sự, với số tiền 3 triệu USD, cựu bộ trưởng phải đối mặt mức án cao nhất là tử hình.

                            Bên cạnh đó, con gái ông Son có thể đối mặt việc bị truy cứu trách nhiệm hình sự về tội Chứa chấp hoặc tiêu thụ tài sản do người khác phạm tội mà có (điều 323 Bộ luật Hình sự 2015). Mức hình phạt cao nhất của tội này lên tới 15 năm tù nếu tài sản, tang vật phạm pháp trị giá một tỷ đồng trở lên.

                            Trường hợp nhận tiền từ cha nhưng không biết nguồn gốc, cô Huyền có thể không bị truy cứu trách nhiệm hình sự. Nhưng ở trường hợp này, cô vẫn phải nộp lại 3 triệu USD theo hình thức thu hồi.

                            Nếu cơ quan điều tra không thu đủ, con ông Son phải nộp bù. Việc này chỉ được coi là "nộp lại tài sản phạm tội mà có", không phải là "khắc phục hậu quả". Ông Son vì thế không được hưởng tình tiết giảm nhẹ "khắc phục hậu quả".

                            Giả thiết thứ hai, ông Son thừa nhận hành vi nhận hối lộ 3 triệu USD song khai không đưa cho con gái mà bản thân tiêu gì cũng không nhớ, lại liên hệ gia đình khắc phục số tiền này thì "số phận pháp lý" lại theo hướng khác.

                            Theo luật sư, ông dùng tài sản riêng của gia đình để nộp lại và khoản này không liên quan tiền nhận hối lộ thì sẽ được coi là "khắc phục hậu quả".

                            Tiền khắc phục hậu quả được nộp ở giai đoạn xét xử sơ thẩm, việc xem xét giảm án tử hình có thể được ghi nhận khi tòa tuyên án sơ thẩm. Nộp sau khi có án sơ thẩm, việc giảm án được xem xét ở cấp phúc thẩm. Khi bản án phúc thẩm có hiệu lực, việc nộp tiền và xem xét ân giảm án sẽ được thực hiện ở quá trình thi hành án.

                            Theo điều 40 Bộ luật Hình sự 2015: Người bị kết án tử hình về tội Tham ô tài sản, Nhận hối lộ mà sau khi bị kết án đã chủ động nộp lại ít nhất 3/4 tài sản phạm tội và hợp tác tích cực với cơ quan chức năng trong việc phát hiện, điều tra, xử lý tội phạm hoặc lập công lớn thì không bị thi hành án tử hình.',
            'foot_content' => 'Phiên sơ thẩm xét xử 14 bị cáo có sai phạm tại dự án MobiFone mua AVG diễn ra tại TAND Hà Nội từ ngày 16 đến 31/12. Trong các bị cáo, hai cựu bộ trưởng Thông tin và Truyền thông là Trương Minh Tuấn, Nguyễn Bắc Son bị truy tố về hai tội Vi phạm các quy định về quản lý đầu tư công gây hậu quả nghiêm trọng (điều 220 Bộ luật Hình sự), Nhận hối lộ (Điều 354) với khung hình phạt 20 năm tù, chung thân hoặc tử hình.

                                Cựu chủ tịch AVG Phạm Nhật Vũ bị xét xử tội Đưa hối lộ (điều 364), đối mặt mức hình phạt 12-20 năm tù.'
            ]
        ]);
    }
}
