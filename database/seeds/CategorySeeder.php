<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('category')->insert([
            [
                'name' => 'News',
                'Description'=> 'Khái niệm luật hình sự nói về những luật có chung tính chất là xác định những hành vi (tội) mà xã hội đó không muốn xảy ra, và đề ra những hình phạt riêng biệt và nặng nề hơn bình thường nếu thành viên xã hội đó phạm vào. ... v.v. luật hình sự nơi nào cũng có.',
                'type' => '0'
            ],
            [
                'name' => 'Blogs',
                'Description'=> 'Khái niệm luật hình sự nói về những luật có chung tính chất là xác định những hành vi (tội) mà xã hội đó không muốn xảy ra, và đề ra những hình phạt riêng biệt và nặng nề hơn bình thường nếu thành viên xã hội đó phạm vào. ... v.v. luật hình sự nơi nào cũng có.',
                'type' => '1'
            ],
            [
                'name' => 'Magazine',
                'Description'=> 'Khái niệm luật hình sự nói về những luật có chung tính chất là xác định những hành vi (tội) mà xã hội đó không muốn xảy ra, và đề ra những hình phạt riêng biệt và nặng nề hơn bình thường nếu thành viên xã hội đó phạm vào. ... v.v. luật hình sự nơi nào cũng có.',
                'type' => '2'
            ],
            [
                'name' => 'Báo Hình Sự',
                'Description'=> 'Khái niệm luật hình sự nói về những luật có chung tính chất là xác định những hành vi (tội) mà xã hội đó không muốn xảy ra, và đề ra những hình phạt riêng biệt và nặng nề hơn bình thường nếu thành viên xã hội đó phạm vào. ... v.v. luật hình sự nơi nào cũng có.',
                'type' => '0'
            ],
            [
                'name' => 'Blog CNTT',
                'Description'=> 'Khái niệm luật hình sự nói về những luật có chung tính chất là xác định những hành vi (tội) mà xã hội đó không muốn xảy ra, và đề ra những hình phạt riêng biệt và nặng nề hơn bình thường nếu thành viên xã hội đó phạm vào. ... v.v. luật hình sự nơi nào cũng có.',
                'type' => '1'
            ],
            [
                'name' => 'Báo Hình Sự',
                'Description'=> 'Khái niệm luật hình sự nói về những luật có chung tính chất là xác định những hành vi (tội) mà xã hội đó không muốn xảy ra, và đề ra những hình phạt riêng biệt và nặng nề hơn bình thường nếu thành viên xã hội đó phạm vào. ... v.v. luật hình sự nơi nào cũng có.',
                'type' => '0'
            ]
        ]);
    }
}
