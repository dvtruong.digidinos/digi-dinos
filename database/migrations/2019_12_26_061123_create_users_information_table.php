<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('users_information', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->timestamp('date_birth');
            $table->boolean('marital');
            $table->integer('number_phone');
            $table->text('address');
            $table->text('description');
            $table->timestamps();

        });
        DB::table('users_information')->insert([
            'user_id' => '1',
            'first_name' => 'admin',
            'last_name' => 'admin',
            'date_birth' => now(),
            'marital' => '1',
            'number_phone' => '123456789',
            'address' => '1234',
            'description' => '12323123'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_information');
    }
}
