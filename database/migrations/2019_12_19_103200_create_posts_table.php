<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function ($table) {
            $table->bigIncrements('id');
            $table->integer('users_id');
            $table->text('title');
            $table->boolean('status');
            $table->biginteger('category_id');
            $table->text('description');
            $table->text('head_content')->nullable();
            $table->text('head_image')->nullable();
            $table->text('content');
            $table->text('content_image')->nullable();
            $table->text('foot_content')->nullable();
            $table->text('foot_image')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
