<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('dashboard.webs.index');
});
Route::get('/register/finish', function() {
  return view('auth.finish', ['message' => 'Waiting for verify!']);
});
Route::post('/send-email', 'Auth\ResetPasswordController@sendMail')->name('password.email');
Route::get('/send-email', function () {
  return view('auth.passwords.email');
})->name('password-reset');
Route::PATCH('reset-password/{token}', 'Auth\ResetPasswordController@resetPassword');
Route::get('reset-password', function() {
  return view('auth.passwords.reset');
});
Route::get('reset-password/finish', function() {
  return view('auth.finishReset');
});
Route::get('/send-email/finish', function() {
  return view('auth.passwords.finish-send-email');
} );

Route::group(['middleware' => ['get.menu']], function () {
  Route::get('/admin', function () {           return view('dashboard.homepage'); })->middleware('auth');

  Route::group(['middleware' => ['role:member']], function () {
    // Route::get('/colors', function () {     return view('dashboard.colors'); });
    // Route::get('/typography', function () { return view('dashboard.typography'); });
    // Route::get('/charts', function () {     return view('dashboard.charts'); });
    // Route::get('/widgets', function () {    return view('dashboard.widgets'); });
    Route::get('/404', function () {        return view('dashboard.404'); });
    Route::get('/500', function () {        return view('dashboard.500'); });
    Route::prefix('admin')->group(function () {
      Route::get('/changepassword', function() {
        return view('dashboard.admin.changePassword');
      });
      Route::PATCH('changepassword', 'UsersController@changePassword')->name('changepassword');
      Route::get('/member/{id}/profile/', 'UsersController@profile')->name('profile');
      Route::PATCH('/member/{id}/profile/', 'UsersController@updateProfile')->name('updateProfile');

      Route::prefix('blog')->group(function () {
        Route::get('/list', 'BlogController@index');
        Route::get('/create', 'BlogController@create');
        Route::post('create', 'BlogController@store')->name('admin.blog.create');
        Route::get('/edit/{id}', 'BlogController@edit');
        Route::PATCH('update/{id}', 'BlogController@update')->name('admin.blog.update');
        Route::delete('delete/{id}', 'BlogController@destroy')->name('admin.blog.delete');
        Route::get('/search', 'BlogController@search')->name('admin.blog.search');
        // Route::get('/collapse', function(){     return view('dashboard.base.collapse'); });

        // Route::get('/forms', function(){        return view('dashboard.base.forms'); });
        // Route::get('/jumbotron', function(){    return view('dashboard.base.jumbotron'); });
        // Route::get('/list-group', function(){   return view('dashboard.base.list-group'); });
        // Route::get('/navs', function(){         return view('dashboard.base.navs'); });

        // Route::get('/pagination', function(){   return view('dashboard.base.pagination'); });
        // Route::get('/popovers', function(){     return view('dashboard.base.popovers'); });
        // Route::get('/progress', function(){     return view('dashboard.base.progress'); });
        // Route::get('/scrollspy', function(){    return view('dashboard.base.scrollspy'); });

        // Route::get('/switches', function(){     return view('dashboard.base.switches'); });
        // Route::get('/tables', function () {     return view('dashboard.base.tables'); });
        // Route::get('/tabs', function () {       return view('dashboard.base.tabs'); });
        // Route::get('/tooltips', function () {   return view('dashboard.base.tooltips'); });
      });
      Route::prefix('category')->group(function () {
        Route::get('/list', 'CategoryController@index');
        Route::get('/search', 'CategoryController@search')->name('admin.category.search');
      });
    });
    // Route::prefix('icon')->group(function () {  // word: "icons" - not working as part of adress
    //     Route::get('/coreui-icons', function(){         return view('dashboard.icons.coreui-icons'); });
    //     Route::get('/flags', function(){                return view('dashboard.icons.flags'); });
    //     Route::get('/brands', function(){               return view('dashboard.icons.brands'); });
    // });
    Route::prefix('notifications')->group(function () {  
      // Route::get('/alerts', function(){   return view('dashboard.notifications.alerts'); });
      // Route::get('/badge', function(){    return view('dashboard.notifications.badge'); });
      // Route::get('/modals', function(){   return view('dashboard.notifications.modals'); });
    });
    Route::resource('notes', 'NotesController');
  });
  Auth::routes();

  Route::group(['middleware' => ['role:admin']], function () {
    Route::resource('admin/member',        'UsersController')->except( ['create', 'store'] );
    Route::resource('/admin/roles',        'RolesController');
    Route::get('admin/roles/move/move-up',      'RolesController@moveUp')->name('roles.up');
    Route::get('admin/roles/move/move-down',    'RolesController@moveDown')->name('roles.down');
    Route::prefix('menu/element')->group(function () {
      Route::get('/',             'MenuElementController@index')->name('menu.index');
      Route::get('/move-up',      'MenuElementController@moveUp')->name('menu.up');
      Route::get('/move-down',    'MenuElementController@moveDown')->name('menu.down');
      Route::get('/create',       'MenuElementController@create')->name('menu.create');
      Route::post('/store',       'MenuElementController@store')->name('menu.store');
      Route::get('/get-parents',  'MenuElementController@getParents');
      Route::get('/edit',         'MenuElementController@edit')->name('menu.edit');
      Route::post('/update',      'MenuElementController@update')->name('menu.update');
      Route::get('/show',         'MenuElementController@show')->name('menu.show');
      Route::get('/delete',       'MenuElementController@delete')->name('menu.delete');
    });
    Route::prefix('menu/menu')->group(function () {
      Route::get('/',         'MenuController@index')->name('menu.menu.index');
      Route::get('/create',   'MenuController@create')->name('menu.menu.create');
      Route::post('/store',   'MenuController@store')->name('menu.menu.store');
      Route::get('/edit',     'MenuController@edit')->name('menu.menu.edit');
      Route::post('/update',  'MenuController@update')->name('menu.menu.update');
      Route::get('/delete',   'MenuController@delete')->name('menu.menu.delete');
    });
    Route::prefix('admin')->group(function () {
      Route::prefix('category')->group(function () {
        Route::get('create', 'CategoryController@create');
        Route::post('create', 'CategoryController@store')->name('admin.category.create');
        Route::get('/edit/{id}', 'CategoryController@edit');
        Route::PATCH('update/{id}', 'CategoryController@update')->name('admin.category.update');
        Route::delete('delete/{id}', 'CategoryController@destroy')->name('admin.category.delete');
      });
    });
  });
});